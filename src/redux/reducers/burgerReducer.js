const stateBurger = {
    topping: [
        { "name": "salad", "price": 10, "amount": 0 },
        { "name": "cheese", "price": 20, "amount": 0 },
        { "name": "beef", "price": 50, "amount": 0 }
    ]
}

const burgerReducer = (state = stateBurger, action) => {
    switch(action.type){
        case "CHANGE_AMOUNT":{
            let index=state.topping.findIndex(topping=>topping.name===action.name);
            if(index!=-1){
                state.topping[index].amount+=action.number;
                if(state.topping[index].amount<0){
                    state.topping[index].amount=0;
                }
                state.topping=[...state.topping];
                return {...state};
            }
        }
    }
    return { ...state }

};

export default burgerReducer