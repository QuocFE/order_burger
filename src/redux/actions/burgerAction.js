import { CHANGE_AMOUNT } from "../types/burgerType"

export const actChangeAmount=(name,number)=>{
    return{
        type:CHANGE_AMOUNT,
        name,
        number
    }
}