import React, { Component } from 'react'
import { connect } from 'react-redux'
import { actChangeAmount } from '../redux/actions/burgerAction'
 class ChooseTopping extends Component {
    renderFoodInfo = () => {
        return this.props.topping.map((topping, index) => {
            return <tr key={index}>
                <th scope="row">{topping.name}</th>
                <td>
                    <button type="button" class="btn btn-danger" onClick={()=>{
                        this.props.changeAmount(topping.name,-1);
                    }}>-</button>
                    {topping.amount}
                    <button type="button" class="btn btn-success" onClick={()=>{
                        this.props.changeAmount(topping.name,1)
                    }}>+</button>
                </td>
                <td>{topping.price}</td>
                <td>{topping.price * topping.amount}</td>
            </tr>
        })
    }
    renderTotalMoney=()=>{
        return this.props.topping.reduce((total,topping)=>{
            return total+=topping.amount * topping.price
        },0)
    }
    render() {
        return (
            <div>
                <h3 className='text-center text-success'>Chọn Thức Ăn</h3>
                <table className="table table-borderless">
                    <thead>
                        <tr>
                            <th scope="col">Thức Ăn</th>
                            <th scope="col">Số Lượng</th>
                            <th scope="col">Đơn Giá</th>
                            <th scope="col">Thành Tiền</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.renderFoodInfo()}
                    </tbody>
                    <tfoot>
                        <th colSpan={3} className='text-end'>Tổng Tiền:{this.renderTotalMoney()}</th>
                    </tfoot>
                </table>
            </div>
        )
    }
}
const mapStateToProp=(state)=>{
    return{
        topping:state.burgerReducer.topping
    }
}
const mapDispatchToProp=(dispatch)=>{
    return{
        changeAmount:(name,number)=>{
            dispatch(actChangeAmount(name,number))
        }
    }
}
export default connect(mapStateToProp,mapDispatchToProp)(ChooseTopping)