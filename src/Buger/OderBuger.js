import React, { Component, PureComponent } from 'react'
import Burger from './Burger'
import './burger.css'
import ChooseTopping from './ChooseTopping'
export default class OderBuger extends PureComponent {
    render() {
        return (
            <div>
                <h1 className='text-warning p-3'>Order Buger</h1>
                <div className='container'>
                    <div className='row'>
                        <div className='col-6'>
                            <Burger></Burger>
                        </div>
                        <div className='col-6'>
                            <ChooseTopping ></ChooseTopping>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
