import React, { Component } from 'react'
import { connect } from 'react-redux'
class Burger extends Component {
    renderTopping = () => {
        return this.props.topping.map((topping, index) => {
            let content = [];
                for (let i = 0; i < topping.amount; i++) {
                    content.push(<div className={topping.name}></div>)
                }
                return <div key={index}>
                    {content}
                </div>
        })
    }
    render() {
        return (
            <div>
                <div className='breadTop'></div>
                <div id='ok'></div>
                {this.renderTopping()}
                <div className='breadBottom'></div>
            </div>
        )
    }
}
const mapStateToProp = (state) => {
    return {
        topping: state.burgerReducer.topping
    }
}
export default connect(mapStateToProp)(Burger)
